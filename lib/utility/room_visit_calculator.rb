module Utility
  class RoomVisitCalculator 
    
    def initialize(data:)
      @no_of_lines = data.shift
      @data = data
      @result = {}
    end

    def call
      data.each do |line|
        parts = line.split(' ')
        if parts.size == 4
          customer_number = parts[0].to_i
          room_number = parts[1].to_i
          direction = parts[2] == 'I' ? :in : :out
          time = parts[3].to_i

          result[room_number] ||= { visitor_count: 0, total_time: 0 } # Initializing Room Object

          if direction == :in
            result[room_number][:total_time] -= time
          else
            result[room_number][:total_time] += time
            result[room_number][:visitor_count] += 1
          end
        end
      end
      #Sorting and finding average of the results
      result.sort.each do |room, results|
        results[:average] = (results[:total_time].to_f / results[:visitor_count]).floor
        results[:s] = results[:visitor_count] > 1 ? 's' : ''
      end
    end
     

    attr_accessor :data, :result, :no_of_lines

  end
end

