class HomeController < ApplicationController
  include Utility
  
  def upload_file
  end

  def compute_result
    if params[:file].present?
      file_data = read_file
      # Calling Gallery Visit Calculator
      @result    = Utility::RoomVisitCalculator.new(data: file_data).call
      respond_to do |format|
        format.html
      end
    else
      redirect_to root_path, alert: "Please attach file before submitting."
    end
   
  end


  private
    def read_file
      file_params.read.split("\n")
    end

    def file_params
      params.fetch(:file)
    end
end
