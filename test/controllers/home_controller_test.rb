require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get upload_file" do
    get home_upload_file_url
    assert_response :success
  end

  test "should get compute_result" do
    get home_compute_result_url
    assert_response :success
  end

end
